package com.gildedrose;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class GildedRoseTest {

    private static final Item[] items = new Item[]{
        new Item("Conjured Mana Cake", 10, 40),
        new Item("Negative Sell In value", -1, 20),
        new Item("Backstage passes to a TAFKAL80ETC concert", -1, 20),
        new Item("Aged Brie", -1, 20),
        new Item("+5 Dexterity Vest", 10, 20),
        new Item("Aged Brie", 2, 0),
        new Item("Elixir of the Mongoose", 5, 7),
        new Item("Sulfuras, Hand of Ragnaros", 0, 80),
        new Item("Sulfuras, Hand of Ragnaros", -1, 80),
        new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20),
        new Item("Backstage passes to a TAFKAL80ETC concert", 10, 48),
        new Item("Backstage passes to a TAFKAL80ETC concert", 5, 47)
    };

    private static final Item[] afterOneDay = new Item[]{
        new Item("Conjured Mana Cake", 9, 38),
        new Item("Negative Sell In value", -2, 18),
        new Item("Backstage passes to a TAFKAL80ETC concert", -2, 0),
        new Item("Aged Brie", -2, 22),
        new Item("+5 Dexterity Vest", 9, 19),
        new Item("Aged Brie", 1, 1),
        new Item("Elixir of the Mongoose", 4, 6),
        new Item("Sulfuras, Hand of Ragnaros", 0, 80),
        new Item("Sulfuras, Hand of Ragnaros", -1, 80),
        new Item("Backstage passes to a TAFKAL80ETC concert", 14, 21),
        new Item("Backstage passes to a TAFKAL80ETC concert", 9, 50),
        new Item("Backstage passes to a TAFKAL80ETC concert", 4, 50)};

    @Test
    void foo() {
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertArrayEquals(afterOneDay, app.items);
    }
}
