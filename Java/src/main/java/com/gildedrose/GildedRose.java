package com.gildedrose;

class GildedRose {
    Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        for (Item item : items) {

            // "Sulfuras", being a legendary item, never has to be sold or decreases in Quality
            if (item.name.equals("Sulfuras, Hand of Ragnaros")) {
                continue;
            }

            item.sellIn--;

            if (item.name.equals("Aged Brie") || item.name.equals("Backstage passes to a TAFKAL80ETC concert")) {
                increaseQuality(item);

                if (item.name.equals("Backstage passes to a TAFKAL80ETC concert")) {
                    // "Backstage passes" Quality increases by 2 when there are 10 days or less
                    // and by 3 when there are 5 days or less
                    if (item.sellIn < 10) {
                        increaseQuality(item);
                    }
                    if (item.sellIn < 5) {
                        increaseQuality(item);
                    }
                }
            } else {
                decreaseQuality(item);
                if (item.name.equals("Conjured Mana Cake")) {
                    // "Conjured" items degrade in Quality twice as fast as normal items
                    decreaseQuality(item);
                }
            }

            if (item.sellIn < 0) {
                if (item.name.equals("Aged Brie")) {
                    // "Aged Brie" actually increases in Quality the older it gets
                    increaseQuality(item);
                } else if (item.name.equals("Backstage passes to a TAFKAL80ETC concert")) {
                    // "Backstage passes" Quality drops to 0 after the concert
                    item.quality = 0;
                } else {
                    // Once the sell by date has passed, Quality degrades twice as fast
                    decreaseQuality(item);
                }
            }
        }
    }

    private void decreaseQuality(Item item) {
        // The Quality of an item is never negative
        if (item.quality > 0) {
            item.quality--;
        }
    }

    private void increaseQuality(Item item) {
        // The Quality of an item is never more than 50
        if (item.quality < 50) {
            item.quality++;
        }
    }
}
